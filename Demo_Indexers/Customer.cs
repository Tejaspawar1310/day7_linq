﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Indexers
{
    internal class Customer
    {
        //fields are by default private
        int id;
        string name;
        string city;

        //constuctor
        public Customer(int id,string name,string city)
        {
            this.id = id;
            this.name = name;
            this.city = city;
        }
        //Access Modifiers then return type then "this" keyword pass parameters[]
        public object this[int index]
        {
            get 
            { if (index == 1) return id;
              else if (index == 2) return name;
              else if (index == 3) return city;
                return null;
            }
            set
            {
                if (index == 1) id = (int) value;
                else if(index == 2) name = (string) value;
                else if(index==3) city = (string) value;
            }
        }
    }
}
