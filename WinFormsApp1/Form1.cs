namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void btnReadFile_Click(object sender, EventArgs e)
        {
            Task<int> task = new Task<int>(CountCharacter);
            task.Start();

            label1.Text = "reading contents from file";
            //CountCharacter();
            int countCharacter = await task;
            label1.Text = countCharacter.ToString()+"numbers of character in file";

            //label1.Text = "reading contents from file";
        }
        private int CountCharacter()
        {
           int countCharacter=0;
           using StreamReader reader = new StreamReader(@"C:\Task\DAY6TASK.txt");
            {
                string content  = reader.ReadToEnd();
                countCharacter = content.Length;
                Thread.Sleep(5000);
            }
            return countCharacter;
        }
    }
}