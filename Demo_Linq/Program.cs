﻿// See https://aka.ms/new-console-template for more information

using Demo_Linq;
using Demo_Linq.Model;

Product[] ProductArray = new Product[]
{
    new Product(){Name="oneplus",Category="MOBILE",Make="ONEPLUSS",Price=2000},
    new Product(){Name="oneplus",Category="MOBILE",Make="applee",Price=10000},
    new Product(){Name="apple",Category="laptop",Make="lenonon",Price=15000},
    new Product(){Name="realme",Category="TV",Make="ONEPLUSS",Price=50000}
};

BestSeller[] bestproduct = new BestSeller[]
{
    new BestSeller(){Name="apple",Rank=1},
    new BestSeller(){Name="oneplus",Rank=2}

};


//print items with the specified range (price>2000 and <20000)
//Product[] productTemp = new Product[5];
//var i = 0;
Console.WriteLine("wiithout LINQ");
foreach(Product item in ProductArray)
{
    if (item.Price > 2000 && item.Price < 20000)
    {
        Console.WriteLine(item);
    }
}
Console.WriteLine("-------WITH METHOD SYNTAX LINQ------");
Product[] products = ProductArray.Where(p => p.Price > 2000 && p.Price < 20000).ToArray();
foreach(Product item in products)
{
    Console.WriteLine(item);
}
Console.WriteLine("--------QUERY SYNTAX---------");
var filterproduct=(from m in ProductArray
where m.Price > 2000 && m.Price < 20000
select m).ToArray();    
foreach(Product item in filterproduct)
{
    Console.WriteLine(item);
}
//Get product by category
Console.WriteLine("----Get product by category------");
Product[] getProductByCategory=ProductArray.Where(p => p.Category =="MOBILE").ToArray();
foreach (Product item in getProductByCategory)
{
    Console.WriteLine(item);
}

//Get First MATCHING value from array()
Console.WriteLine("----get first mttching value----");
Product firstoccurence=ProductArray.FirstOrDefault(p => p.Name == "oneplus");
Console.WriteLine(firstoccurence);

//order by
Console.WriteLine("-------ORDER BY LOGIC-----");
var desc = (from p in ProductArray
 orderby p.Price descending
 select p);
foreach(Product item in desc)
{
Console.WriteLine(item);
}
//Grouping operation
Console.WriteLine("-----Grouping Operation----");
var groupbycategory = (from p in ProductArray
                       group p by p.Category);
foreach(var item in groupbycategory)
{
    Console.WriteLine($"Group by category::{item.Key}");
    foreach(Product item1 in item)
    {
        Console.WriteLine(item1);
    }
}
//JOIN OPERATION
//product name and product rank
Console.WriteLine("---------JOIN OPERATION---------");
var result = (from b in bestproduct
                     join p in ProductArray
                     on b.Name equals p.Name
                     select new
                     {
                         productName = p.Name,
                         bestproductname = b.Rank
                     });
foreach(var item in result)
{
    Console.WriteLine(item.productName+"\t"+"Rank::" +item.bestproductname);
}

//Maxby()
Console.WriteLine("-----Maxby() -----");
var highestprice = ProductArray.MaxBy(p => p.Price);
Console.WriteLine(highestprice);
// Minby()
Console.WriteLine("-----Min by() -----");
var minprice = ProductArray.MinBy(p => p.Price);
Console.WriteLine(minprice);
//count elements
Console.WriteLine("-----count elements-----");
var countItems = ProductArray.Count();
Console.WriteLine($"Total items in cart are {countItems}");
Console.WriteLine("-----Even values in list-----");
List<int> myValues = new List<int> { 101, 102, 103, 104, 105 };
int getevencount =myValues.Count(e=>e%2==0);
Console.WriteLine($"Even numbers in list are total {getevencount}");
Console.WriteLine("-------contais b in city------");
string[] cities = { "bumbai", "delhi", "pune", "mysore", "bhopal" };
var cityresult = (from city in cities
                  where city.Contains("b")
                  select city);
foreach(var c in cityresult)
{
    Console.WriteLine(c);
}
Console.WriteLine("---take function---");
var result2 = cities.Take(3);
foreach(string city in result2)
{
    Console.WriteLine(city);
}

