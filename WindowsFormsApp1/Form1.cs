﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_LongRunning_Click(object sender, EventArgs e)
        {
            btn_LongRunning.Enabled = false;
            btn_PrintValues.Enabled = false;
            Thread anotherThread = new Thread(PerformSomeTask);
            anotherThread.Start();

            btn_LongRunning.Enabled = true;
            btn_PrintValues.Enabled = true;


        }
        private void PerformSomeTask()
        {
            Thread.Sleep(5000);
        }
        private void btn_PrintValues_Click(object sender, EventArgs e)
        {
            for(int i=0; i < 10; i++)
            {
                listBox1.Items.Add(i);
            }
        }
    }
}
